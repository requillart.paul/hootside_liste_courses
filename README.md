Pour utiliser le projet 

1. cloner le repository
2. lancer la commande "composer install"
3. parametrer la base de données dans le .env
4. créer la base de données avec la commande "php bin/console doctrine:database:create"


Le projet utilise les CDN pour bootstrap et jQuery à cause d'un bug qui m'a empeché d'utiliser Webpack Encore

Pour créer un compte admin : 
1. créer un compte utilisateur
2. ajouter ["ROLE_ADMIN"] dans la colonne rôle dans la base de données


*Il n'est pas possible d'ajouter une image dans une liste pour le moment*

*Il n'est pas possible de faire du drag and drop avec les listes*