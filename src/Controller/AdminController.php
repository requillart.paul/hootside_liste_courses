<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\UserRepository;
use App\Entity\User;

class AdminController extends AbstractController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(UserRepository $userRepository)
    {
        $users = $userRepository->findAll();
        return $this->render('admin/index.html.twig', [
            'users' => $users
        ]);
    }


    /**
     * @Route("/admin/user/{id}", name="admin_user")
     */
    public function showUser(User $user){
        return $this->render('admin/user.html.twig', [
            'user' => $user
        ]);
    }
}
