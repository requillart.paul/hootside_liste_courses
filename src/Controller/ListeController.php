<?php

namespace App\Controller;

use App\Entity\Liste;
use App\Form\ListeType;
use App\Repository\ListeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Common\Collections\ArrayCollection;

class ListeController extends AbstractController
{
    /**
     * @Route("/", name="liste_index", methods={"GET"})
     */
    public function index(ListeRepository $listeRepository): Response
    {

        $user = $this->getUser();
        $listes = $listeRepository->findAllByUser($user);

        $user->setRoles(['ROLE_ADMIN']);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->flush();
        return $this->render('liste/index.html.twig', [
            'listes' => $listes,
        ]);
    }

    /**
     * @Route("/liste/new", name="liste_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $liste = new Liste();
        $form = $this->createForm(ListeType::class, $liste);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $liste->setAuthor($this->getUser());
            $entityManager->persist($liste);
            $entityManager->flush();

            return $this->redirectToRoute('liste_index');
        }

        return $this->render('liste/new.html.twig', [
            'liste' => $liste,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/liste/{id}", name="liste_show", methods={"GET","POST"})
     */
    public function show(Liste $liste, Request $request): Response
    {
        $form = $this->createForm(ListeType::class, $liste);
        $form->handleRequest($request);

        $originalItems = new ArrayCollection();

        // Create an ArrayCollection of the current Tag objects in the database
        foreach ($liste->getItems() as $item) {
            $originalItems->add($item);
        }

        if ($form->isSubmitted() && $form->isValid()) {
            foreach ($originalItems as $item) {
                if (false === $liste->getItems()->contains($item)) {
                    // remove the Task from the Tag
                    $item->getListe()->removeElement($liste);
    
                    // if it was a many-to-one relationship, remove the relationship like this
                    // $tag->setTask(null);
    
                    $entityManager->persist($item);
    
                    // if you wanted to delete the Tag entirely, you can also do that
                    // $entityManager->remove($tag);
                }
            }

            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('liste_index');
        }

        return $this->render('liste/show.html.twig', [
            'liste' => $liste,
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/liste/{id}", name="liste_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Liste $liste): Response
    {
        if ($this->isCsrfTokenValid('delete'.$liste->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($liste);
            $entityManager->flush();
        }

        return $this->redirectToRoute('liste_index');
    }
}
